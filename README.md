# How to change your Git remote configuration

### Resources
- [Git remote set-url documentation](https://git-scm.com/docs/git-remote#Documentation/git-remote.txt-emset-urlem)
- [Youtube example video](https://www.youtube.com/watch?v=66rzn4vxHok)

### Introduction
- Git defines where you will fetch(pull) and push to based on the configuration of your `.git/config` file.
- This configuration tells git the location of the remote(online) repository that you want to connect to.

### Steps
1. [View your current Git remote configuration](#viewing-the-current-git-remote-configuration-of-your-local-repository)
2. Configure your preferred method of GitLab authentication `ON THE NEW REMOTE REPOSITORY`
   - [SSH Key Pair Authentication](#setting-your-gitlab-authentication-method-for-ssh-authentication-using-rsa-key-pairs)
   - [GitLab PERSONAL ACCESS TOKEN](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#create-a-personal-access-token)
3. Configure your IDE for your preferred method of GitLab Authentication
   - Refer to documentation for your IDE
4. [Change the remote for your local project]()
5. [View your current Git remote configuration again to verify that the change took effect](#viewing-the-current-git-remote-configuration-of-your-local-repository)

### Viewing the current Git remote configuration of your local repository
- You can view the contents of this file directly from the root level of the project on your local repository. Specifically you are looking for the configurations set in the section titled `[remote "origin"]`
  - Command:
    ```
    cat .git/conifg
    ```
  - Example output:
    ```
    [core]
            repositoryformatversion = 0
            filemode = false
            bare = false
            logallrefupdates = true
            symlinks = false
            ignorecase = true
    [remote "origin"]
            url = https://gitlab.com/my-awesome-project.git
            fetch = +refs/heads/*:refs/remotes/origin/*
    [branch "master"]
            remote = origin
            merge = refs/heads/master
    ```
- There is a much easier way to view this data from the command line without looking at the contents of this file:
  - Command:
    ```
    git remote -v
    ```
  - Example output:
    ```
    origin  https://gitlab.com/my-awesome-project.git (fetch)
    origin  https://gitlab.com/my-awesome-project.git (push)
    ```
    > As you can see, both the fetch(pull) and the push are pointing at the same remote repository.  This is correct for our use case.

### Changing the remote for a git local repository
- Before changing your local repository's remote, you should ensure that you first setup your method of authentication to the new repository either using `GitLab PERSONAL ACCESS TOKENS` or `SSH KEYPAIRS`.
- Once you have completed the configuration of your authentication, return to your local repository and use the following syntax to change point your local repository to a different remote repository:
  - Syntax:
    ```
    git remote set-url origin <url_of_remote_repo_to_change_to>
    ```
  - Example for a GitLab SSH authentication setup:
    ```
    git remote set-url origin ssh://git@gitlab.com/my-awesome-project.git
    ```
  - Example for a GitLab PERSONAL ACCESS TOKEN authentication setup:
    ```
    git remote set-url origin https://gitlab.com/my-awesome-project.git
    ```

github.com/my-awesome-project.git
### Setting your GitLab authentication method for SSH authentication using RSA key pairs
- Open a terminal window on your development computer
- Navigate to your `.ssh` directory
  ```
  cd ~/.ssh
  ```
- Generate an RSA key pair:
  ```
  ssh-keygen -t rsa -b 4096
  ```
- When prompted with `"Enter file in which to save the key..."` enter the key name `gdn-key` and press enter.  This name is arbitrary and may be set to anything you like, but you will need it later.
- A passphrase is optional, but not required.  You may simply press enter when prompted for a passphrase.
- After the passphrase prompts you should see the following indicating an SSH key pair was successfully created in the `~/.ssh` directory:
  ```
  Your public key has been saved in gdn-key.pub
  The key fingerprint is:
  SHA256:Sw/UdLtKF9DJOCw8ReVDiE4VFYWomwQTE1t7krApjc4 johna@Dell_Laptop
  The key's randomart image is:
  +---[RSA 4096]----+
  |      =+.*BX*=.  |
  |     ooB*=*==.   |
  |    o ===+o.=    |
  |   o . .+o   +   |
  |    E  .So. o    |
  |       .o= o     |
  |        . o      |
  |                 |
  |                 |
  +----[SHA256]-----+
  ```
- View the contents of you `~/.ssh` directory to veriry that your public and private SSH keys were created:
  - Command:
    ```
    ls
    ```
  - Output:
    ```
    gdn-key  gdn-key.pub
    ```
    > There may be other files in this directory, but you are just checking that these two exist.
- Display the contents of the gdn-key.pub file:
  - Command:
    ```
    cat gdn-key.pub
    ```
  - Example output:
    ```
    ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQChvZexU/+aElxPpC6VxktS+hegXhT8K+8j/w1G6ukabiPijyN1SgYokFuYuVh9PpCTRGRZ8TMNguidY1mZQyQKcjkQ0zhY3fHdMBipQXt5O1jycTKvmwtn6QHqeVILpT56FKaHFWwhbMVo2fjyLBsgWJi403A/nV/Tfv98dXf8+/rxEkiAWO+LY8TFCE7Dgbdk6sahB7eXGLokm41OnqjDBH06/8/SFA2D9aF+kFyx6uA9dVUMgcyilyxzMOdEaddV3FMCCVuexIe+upXaj0QIC/bkAn0Pn6pHCwO1Air37q3Mdz20V6em7hfKT+3ij2cdCCK8T6SKV2UPhPTXcXFoiq5fUE8IBJlJ3yFTAbPR8k+JQ0MjQayed5YlCDHMA5apwC4MX8z6OnbkR/45BNyAnXE4ZwgNoi6nTKrd8QnV5GgN/ag1X32QcM+h6VDmMQUb4rdxftjVU0xVnG6WxGXPJgH1GzHgKNow//E2kaQyocF5Vq4OwS31qORVlzjbmLW5kVEgXMBjohO7jrFm7pNPr1I5IMXJDDB32+Qwnpn+fesfdgI4enVbB7siSn1s5HFw/0b23dZkK61ng/XtRhxS5lPztKlNhKHfSMK/BRfC96/Iiciyi4KsiBP76NYBKZLbupkJJp7Nn+gBWfxlzEpy8UjqpEeGpgLpYCeZ1YW/vw== johna@Dell_Laptop
    ```
    > Your output will be different
- Highlight and copy all of this text.  You will use it in the next step.
- With the contents of your gdn-key.pub copied, you can follow the remaining GitLab directions on the link below starting at step #2:
  - [Add an SSH key to your GitLab account](https://docs.gitlab.com/ee/user/ssh.html#add-an-ssh-key-to-your-gitlab-account)
- Additionally, you may verify the SSH authentication to GitLab from your development laptop using these steps on the GitLab site:
  - [Verify that you can connect](https://docs.gitlab.com/ee/user/ssh.html#verify-that-you-can-connect)
